#!/bin/bash
from=""
to=""
data=""
subject=""
usage() {
    echo "Usage: $( basename "$0" ) [options]"
    echo "Options:"
    echo "    -d|--data <MailContent>:     Mail"
    echo "    -t|--to <MailAddress>:       Recipient mailaddress"
    echo "    -f|--from <MailAddress>:     From mailaddress"
    echo "    -s|--subject <Subject>:      Subject"
    echo "    -h|--help:                   This help"
    exit 0
}

for arg in "$@"; do
    case "$arg" in
        -h|--help)       shift ; usage     ;;
        -d|--data)     shift ; current=data ;;
        -t|--to)    shift ; current=to ;;
        -s|--subject)    shift ; current=subject ;;
        -f|--from)          shift ; current=from   ;;
        --) shift ; break ;;
        -*) echo -e >&2 "$( basename "$0" ): Invalid option \`$arg' found\n" && usage ;;
        *) [ -n "$current" ] && eval "$current='$arg'" && current="" && shift || break ;;
    esac
done

if [ -z "$from" ] || [ -z "$to" ] || [ -z "$data" ]
then
    usage
fi

echo "HELO mail.uni-ulm.de
MAIL FROM: <$from>
RCPT TO: <$to>
DATA
Subject: $subject
Date: `date -R`
From: <$from>
To: <$to>

$mail

.

" | nc mail.uni-ulm.de 25
